<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "event".
 *
 * @property int $idEvent
 * @property string $event_name
 * @property string $start_date
 * @property string $end_date
 * @property int $breaks
 * @property int $material
 * @property int $break_quantity
 *
 * @property Conference[] $conferences
 * @property EventRegistration[] $eventRegistrations
 * @property Attendant[] $attendants
 * @property UserEvent[] $userEvents
 * @property User[] $users
 */


class Event extends \yii\db\ActiveRecord
{
    public $banner;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_name', 'start_date', 'end_date', 'breaks', 'material'], 'required'],
            [['start_date', 'end_date'], 'safe'],
            [['breaks', 'material', 'break_quantity'], 'integer'],
            [['event_name'], 'string', 'max' => 250],
            ['start_date','validateDates'],
            [['banner'], 'file', 'extensions' => 'png, jpg'],
            ['break_quantity', 'integer', 'min' => 0],
        ];
    }

    public function validateDates(){
    if(strtotime($this->end_date) <= strtotime($this->start_date)){
        $this->addError('start_date','Please give correct Start and End dates');
        $this->addError('end_date','Please give correct Start and End dates');
    }
}

public function upload()
    {
        if ($this->validate()) {
            $this->banner->saveAs('uploads/' . $this->banner->baseName . '.' . $this->banner->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idEvent' => 'Id Event',
            'event_name' => 'Nombre Evento',
            'start_date' => 'Fecha Inicio',
            'end_date' => 'Fecha Fin',
            'breaks' => 'Breaks',
            'material' => 'Material',
            'break_quantity' => 'Cantidad Break',
            'banner' => 'Imagen',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConferences()
    {
        return $this->hasMany(Conference::className(), ['idEvent' => 'idEvent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRegistrations()
    {
        return $this->hasMany(EventRegistration::className(), ['idEvent' => 'idEvent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendants()
    {
        return $this->hasMany(Attendant::className(), ['idAttendant' => 'idAttendant'])->viaTable('event_registration', ['idEvent' => 'idEvent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEvents()
    {
        return $this->hasMany(UserEvent::className(), ['idEvent' => 'idEvent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'idUser'])->viaTable('user_event', ['idEvent' => 'idEvent']);
    }

    public function getMaterialLabel()
    {
        return $this->material ? 'Yes' : 'No';
    }

    public function getBreakLabel()
    {
        return $this->breaks ? 'Yes' : 'No';
    }

    public function getTotalBreaks(){
        if(EventRegistration::find()->where(['idEvent' => $this->idEvent])->sum('break_number')){
            return EventRegistration::find()->where(['idEvent' => $this->idEvent])->sum('break_number');
        }else{
            return 0;
        }
    }

    public function getBreaksOverplus(){
        return (self::getTotalAttendants() * $this->break_quantity) - self::getTotalBreaks();
    }

    public function getTotalAttendants(){
        return EventRegistration::find()->where(['idEvent' => $this->idEvent])->count();
    }

    public function getTotalMaterial(){
        return EventRegistration::find()->where(['idEvent' => $this->idEvent])->andWhere(['material'=>'1'])->count();
    }

    public function getMissingMaterial(){
        return self::getTotalAttendants() - self::getTotalMaterial();
    }

    public function getTotalAttedantsBreaksZero(){
        return EventRegistration::find()->where(['idEvent' => $this->idEvent])->andWhere(['break_number'=>'0'])->count();
    }
    public function getTotalAttedantsBreaks(){
        return EventRegistration::find()->where(['idEvent' => $this->idEvent])->andWhere(['<','break_number',$this->break_quantity])->andWhere(['>','break_number',0])->count();
    }
    public function getTotalAttedantsBreaksFull(){
        return EventRegistration::find()->where(['idEvent' => $this->idEvent])->andWhere(['break_number'=>$this->break_quantity])->count();
    }

    public function actionSample() {
        $out = [];        
        $idEvent = end($_POST['depdrop_parents']);
        $list = EventRegistration::find()->andWhere(['idEvent' => $idEvent])->asArray()->all();
        $selected  = null;
        if ($idEvent != null && count($list) > 0) {
            $selected = '';
            foreach ($list as $i => $registration) {
                $out[] = ['id' => $registration['idAttendant'], 'name' => $registration['barcode']];
            }

            echo Json::encode(['output' => $out, 'selected'=>$selected]);
            return;
        }
        
        echo Json::encode(['output' => '', 'selected'=>'']);
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Event;
use yii\helpers\ArrayHelper;

/**
 * EventSearch represents the model behind the search form of `app\models\Event`.
 */
class EventSearch extends Event
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEvent', 'breaks', 'material', 'break_quantity'], 'integer'],
            [['event_name', 'start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find();

        // add conditions that should always apply here
        if(!Yii::$app->user->can('Ver Todos')){
            if(Yii::$app->user->can('Ver en Evento')){
                $time = new \DateTime('now');
                $today = $time->format('Y-m-d h:i:s');
                $total = Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->count();
                if(!$total==0){
                    $events=Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->all();
                    $ids = ArrayHelper::getColumn($events, 'idEvent');
                    $query->andFilterWhere(['idEvent'=>$ids]);
                }else{
                    $query->andFilterWhere(['idEvent'=>0]);
                }
                
            }else{
                $query->andFilterWhere(['idEvent'=>0]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idEvent' => $this->idEvent,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'breaks' => $this->breaks,
            'material' => $this->material,
            'break_quantity' => $this->break_quantity,
        ]);

        $query->andFilterWhere(['like', 'event_name', $this->event_name]);

        return $dataProvider;
    }
}

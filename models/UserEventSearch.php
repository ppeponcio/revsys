<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserEvent;
use app\models\Event;
use yii\helpers\ArrayHelper;
/**
 * UserEventSearch represents the model behind the search form of `app\models\UserEvent`.
 */
class UserEventSearch extends UserEvent
{
    /**
     * {@inheritdoc}
     */
    public $event;
    public $user;
    public $userRegistry;
    public function rules()
    {
        return [
            [['idUser', 'idEvent', 'idUserRegistry'], 'integer'],
            [['dateTime'], 'safe'],
            [['event','user','userRegistry'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserEvent::find();

        $query->joinWith(['event','user']);

        // add conditions that should always apply here
        if(!Yii::$app->user->can('Ver Todos')){
            if(Yii::$app->user->can('Ver en Evento')){
                $time = new \DateTime('now');
                $today = $time->format('Y-m-d h:i:s');
                $total = Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->count();
                if(!$total==0){
                    $events=Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->all();
                    $ids = ArrayHelper::getColumn($events, 'idEvent');
                    $query->andFilterWhere(['event.idEvent'=>$ids]);
                }else{
                    $query->andFilterWhere(['event.idEvent'=>0]);
                }
                
            }else{

                $query->andFilterWhere(['event.idEvent'=>0]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['event'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['event.event_name' => SORT_ASC],
        'desc' => ['event.event_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['user'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['user.username' => SORT_ASC],
        'desc' => ['user.username' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['userRegistry'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['userRegistry.username' => SORT_ASC],
        'desc' => ['userRegistry.username' => SORT_DESC],
        ];

        // $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idUser' => $this->idUser,
            'idEvent' => $this->idEvent,
            'idUserRegistry' => $this->idUserRegistry,
            'dateTime' => $this->dateTime,
        ]);

        $query->andFilterWhere(['like', 'event.event_name', $this->event])
        ->andFilterWhere(['like', 'user.username', $this->user])
        ->andFilterWhere(['like', 'user.username', $this->userRegistry]);

        return $dataProvider;
    }
}

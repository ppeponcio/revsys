<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "carrera".
 *
 * @property int $idCarrera
 * @property string $carrer_name
 *
 * @property Attendant[] $attendants
 */
class Carrera extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carrera';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['carrer_name'], 'required'],
            [['carrer_name'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCarrera' => 'Id Carrera',
            'carrer_name' => 'Nombre Carrera',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendants()
    {
        return $this->hasMany(Attendant::className(), ['idCarrera' => 'idCarrera']);
    }
}

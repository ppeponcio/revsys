<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Attendant;

/**
 * AttendantSearch represents the model behind the search form of `app\models\Attendant`.
 */
class AttendantSearch extends Attendant
{
    /**
     * {@inheritdoc}
     */
    public $carrera;
    public function rules()
    {
        return [
            [['idAttendant', 'idCarrera'], 'integer'],
            [['passport', 'first_name', 'last_name'], 'safe'],
            [['carrera'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attendant::find();

        $query->joinWith(['carrera']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['carrera'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['carrera.carrer_name' => SORT_ASC],
        'desc' => ['carrera.carrer_name' => SORT_DESC],
        ];

        //$this->load($params);

        /*if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }*/

        if (!($this->load($params) && $this->validate())) {
        return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idAttendant' => $this->idAttendant,
            'idCarrera' => $this->idCarrera,
        ]);

        $query->andFilterWhere(['like', 'passport', $this->passport])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'carrera.carrer_name', $this->carrera]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attendance_record_conference".
 *
 * @property int $idAttendant
 * @property int $idConference
 * @property string $registration_date
 * @property int $idUser
 *
 * @property Event $event
 * @property Attendant $attendant
 * @property Conference $conference
 * @property User $user
 */
class AttendanceRecordConference extends \yii\db\ActiveRecord
{
    public $event_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance_record_conference';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idAttendant', 'idConference', 'registration_date', 'idUser'], 'required'],
            [['idAttendant', 'idConference', 'idUser'], 'integer'],
            [['event_name'], 'string'],
            [['registration_date','event_name'], 'safe'],
            [['idAttendant', 'idConference'], 'unique', 'targetAttribute' => ['idAttendant', 'idConference']],
            [['idAttendant'], 'exist', 'skipOnError' => true, 'targetClass' => Attendant::className(), 'targetAttribute' => ['idAttendant' => 'idAttendant']],
            [['idConference'], 'exist', 'skipOnError' => true, 'targetClass' => Conference::className(), 'targetAttribute' => ['idConference' => 'idConference']],
            [['idUser'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['idUser' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idAttendant' => 'Id Attendant',
            'idConference' => 'Id Conference',
            'registration_date' => 'Fecha de Registro',
            'idUser' => 'Id User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendant()
    {
        return $this->hasOne(Attendant::className(), ['idAttendant' => 'idAttendant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConference()
    {
        return $this->hasOne(Conference::className(), ['idConference' => 'idConference']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'idUser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['idEvent' => 'idEvent'])->viaTable('conference', ['idConference' => 'idConference']);
    }

    public function getDetails(){
        return $this->conference->conference_name+' - '+$this->registration_date;
    }

}

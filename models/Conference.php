<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conference".
 *
 * @property int $idConference
 * @property string $conference_name
 * @property int $idEvent
 * @property string $hora_inicio
 * @property string $hora_fin
 *
 * @property AttendanceRecordConference[] $attendanceRecordConferences
 * @property Attendant[] $attendants
 * @property Event $event
 */
class Conference extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conference';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['conference_name', 'idEvent','hora_inicio','hora_fin'], 'required'],
            [['idEvent'], 'integer'],
            ['hora_inicio','validateDates'],
            [['conference_name'], 'string', 'max' => 250],
            [['idEvent'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['idEvent' => 'idEvent']],
        ];
    }

    public function validateDates(){
        if(strtotime($this->hora_fin) <= strtotime($this->hora_inicio)){
            $this->addError('hora_inicio','Por favor verifique que la hora de inicio se menor que la de finalización');
            $this->addError('hora_fin','Por favor verifique que la hora de inicio se menor que la de finalización');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idConference' => 'Id Conference',
            'conference_name' => 'Nombre Conferencia',
            'hora_inicio' =>'Hora inicio',
            'hora_fin' =>' Hora fin',
            'idEvent' => 'Id Event',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendanceRecordConferences()
    {
        return $this->hasMany(AttendanceRecordConference::className(), ['idConference' => 'idConference']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendants()
    {
        return $this->hasMany(Attendant::className(), ['idAttendant' => 'idAttendant'])->viaTable('attendance_record_conference', ['idConference' => 'idConference']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['idEvent' => 'idEvent']);
    }

    public function getDetails()
    {
        return $this->hasOne(Event::className(), ['idEvent' => 'idEvent']);
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AttendanceRecordConference;
use app\models\Event;
use yii\helpers\ArrayHelper;

/**
 * AttendanceRecordConferenceSearch represents the model behind the search form of `app\models\AttendanceRecordConference`.
 */
class AttendanceRecordConferenceSearch extends AttendanceRecordConference
{
    /**
     * {@inheritdoc}
     */
    public $attendant;
    public $user;
    public $conference;
    public function rules()
    {
        return [
            [['idAttendant', 'idConference', 'idUser'], 'integer'],
            [['registration_date','event_name'], 'safe'],
            [['attendant','conference', 'user'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AttendanceRecordConference::find();

        // add conditions that should always apply here
        // if(!Yii::$app->user->can('Ver Todos')){
        //     $time = new \DateTime('now');
        //     $today = $time->format('Y-m-d h:i:s');
        //     $events=Event::find()->joinWith(["userEvents eu"],true,"INNER JOIN")->select('event.idEvent')->andWhere(["eu.idUser"=>Yii::$app->user->id])->all();
        //     $query->andFilterWhere(['in','idEvent', $events,]);
        //     $query->andFilterWhere(['idUser'=>Yii::$app->user->id,]);
        // }

        if(!Yii::$app->user->can('Ver Todos')){
            if(Yii::$app->user->can('Ver en Evento')){
                $time = new \DateTime('now');
                $today = $time->format('Y-m-d h:i:s');
                $events=Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->all();
                $total = Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->count();
                if(!$total==0){
                    $ids = ArrayHelper::getColumn($events, 'idEvent');
                    $query->andFilterWhere(['conference.idEvent'=>$ids]);
                }else{
                    $query->andFilterWhere(['conference.idEvent'=>0]);
                }
                

            }else{
                $time = new \DateTime('now');
                $today = $time->format('Y-m-d h:i:s');
                $events=Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->all();
                $ids = ArrayHelper::getColumn($events, 'idEvent');
                $total = Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->count();
                if(!$total==0){
                    $ids = ArrayHelper::getColumn($events, 'idEvent');
                    $query->andFilterWhere(['conference.idEvent'=>$ids]);
                    $query->andFilterWhere(['idUser'=>Yii::$app->user->id,]);
                }else{
                    $query->andFilterWhere(['conference.idEvent'=>0]);
                }
                
            }
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['conference','user','attendant','conference.event']);

        $dataProvider->sort->attributes['conference'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['conference.conference_name' => SORT_ASC],
        'desc' => ['conference.conference_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['user'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['user.username' => SORT_ASC],
        'desc' => ['user.username' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['attendant'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['attendant.passport' => SORT_ASC],
        'desc' => ['attendant.passport' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['event_name'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['event.event_name' => SORT_ASC],
        'desc' => ['event.event_name' => SORT_DESC],
        ];

        // $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idAttendant' => $this->idAttendant,
            'idConference' => $this->idConference,
            'registration_date' => $this->registration_date,
            'idUser' => $this->idUser,
            'event.event_name' => $this->event_name,
        ]);

        $query->andFilterWhere(['like', 'conference.conference_name', $this->conference])
        ->andFilterWhere(['like', 'user.username', $this->user])
        ->andFilterWhere(['like', 'event.event_name', $this->event])
        ->andFilterWhere(['like', 'attendant.passport', $this->attendant]);



        return $dataProvider;
    }
}

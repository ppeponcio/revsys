<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attendant".
 *
 * @property int $idAttendant
 * @property string $passport
 * @property int $idCarrera
 * @property string $first_name
 * @property string $last_name
 *
 * @property AttendanceRecordConference[] $attendanceRecordConferences
 * @property Conference[] $conferences
 * @property Carrera $carrera
 * @property EventRegistration[] $eventRegistrations
 * @property Event[] $events
 */
class Attendant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['passport', 'idCarrera', 'first_name', 'last_name'], 'required'],
            [['idCarrera'], 'integer'],
            [['passport'], 'string', 'max' => 30],
            [['first_name', 'last_name'], 'string', 'max' => 250],
            [['passport'], 'unique'],
            [['idCarrera'], 'exist', 'skipOnError' => true, 'targetClass' => Carrera::className(), 'targetAttribute' => ['idCarrera' => 'idCarrera']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idAttendant' => 'Id Attendant',
            'passport' => 'Pasaporte',
            'idCarrera' => 'Id Carrera',
            'first_name' => 'Nombre',
            'last_name' => 'Apellido',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendanceRecordConferences()
    {
        return $this->hasMany(AttendanceRecordConference::className(), ['idAttendant' => 'idAttendant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConferences()
    {
        return $this->hasMany(Conference::className(), ['idConference' => 'idConference'])->viaTable('attendance_record_conference', ['idAttendant' => 'idAttendant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarrera()
    {
        return $this->hasOne(Carrera::className(), ['idCarrera' => 'idCarrera']);
    }

    public function setCarrera($value)
    {
        $this->carrera =  Carrera::find()->where(['carrer_name' => $value])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRegistrations()
    {
        return $this->hasMany(EventRegistration::className(), ['idAttendant' => 'idAttendant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['idEvent' => 'idEvent'])->viaTable('event_registration', ['idAttendant' => 'idAttendant']);
    }
}

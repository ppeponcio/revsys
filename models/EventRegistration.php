<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_registration".
 *
 * @property int $idAttendant
 * @property int $idEvent
 * @property string $registration_date
 * @property int $idUser
 * @property int $barcode
 * @property int $material
 * @property int $break_number
 *
 * @property Attendant $attendant
 * @property Event $event
 * @property User $user
 */
class EventRegistration extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_registration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idAttendant', 'idEvent', 'registration_date', 'idUser', 'barcode', 'material'], 'required'],
            [['idAttendant', 'idEvent', 'idUser', 'barcode', 'material', 'break_number'], 'integer'],
            [['registration_date'], 'safe'],
            [['idAttendant', 'idEvent'], 'unique', 'targetAttribute' => ['idAttendant', 'idEvent']],
            [['idEvent','barcode'], 'unique','targetAttribute' => ['idEvent','barcode']],
            [['idAttendant'], 'exist', 'skipOnError' => true, 'targetClass' => Attendant::className(), 'targetAttribute' => ['idAttendant' => 'idAttendant']],
            [['idEvent'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['idEvent' => 'idEvent']],
            [['idUser'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['idUser' => 'id']],
            //[['break_number'], 'validateNumber'],
            [['break_number'], 'validateNumber', 'when' => function ($model) {
                return $model->event->breaks === 1;
            }],
            [['break_number'], 'validateBreak','params'=>['event'=>'event'], 'when' => function ($model) {
                return $model->event->breaks === 0;
            }],
        ];
    }

    public function validateNumber($attribute, $params, $validator)
    {
        if ($this->$attribute > $this->event->break_quantity) {
            $this->addError($attribute, 'Numero de breaks maximo alcanzado');
        }
    }

    public function validateBreak($attribute, $params, $validator)
    {
        if ($this->$attribute > 0) {
            $this->addError($attribute, 'No se habilitaron breaks para este evento');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idAttendant' => 'Id Attendant',
            'idEvent' => 'Id Event',
            'registration_date' => 'Fecha Registro',
            'idUser' => 'Id User',
            'barcode' => 'Código',
            'material' => 'Material',
            'break_number' => 'Número Break',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendant()
    {
        return $this->hasOne(Attendant::className(), ['idAttendant' => 'idAttendant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['idEvent' => 'idEvent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'idUser']);
    }

}

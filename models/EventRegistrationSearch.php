<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventRegistration;
use app\models\Event;
use yii\helpers\ArrayHelper;

/**
 * EventRegistrationSearch represents the model behind the search form of `app\models\EventRegistration`.
 */
class EventRegistrationSearch extends EventRegistration
{
    /**
     * {@inheritdoc}
     */
    public $attendant;
    public $user;
    public $event;
    public function rules()
    {
        return [
            [['idAttendant', 'idEvent', 'idUser', 'barcode', 'material', 'break_number'], 'integer'],
            [['registration_date'], 'safe'],
            [['attendant','event', 'user'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventRegistration::find();

        // add conditions that should always apply here
        if(!Yii::$app->user->can('Ver Todos')){
            if(Yii::$app->user->can('Ver en Evento')){
                $time = new \DateTime('now');
                $today = $time->format('Y-m-d h:i:s');
                $events=Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->all();
                $total = Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->count();
                if(!$total==0){
                    $ids = ArrayHelper::getColumn($events, 'idEvent');
                    $query->andFilterWhere(['event.idEvent'=>$ids]);
                }else{
                    $query->andFilterWhere(['event.idEvent'=>0]);
                }
                

            }else{
                $time = new \DateTime('now');
                $today = $time->format('Y-m-d h:i:s');
                $events=Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->all();
                $ids = ArrayHelper::getColumn($events, 'idEvent');
                $total = Event::find()->joinWith(["userEvents ue"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["ue.idUser"=>Yii::$app->user->id])->count();
                if(!$total==0){
                    $ids = ArrayHelper::getColumn($events, 'idEvent');
                    $query->andFilterWhere(['event.idEvent'=>$ids]);
                    $query->andFilterWhere(['idUser'=>Yii::$app->user->id,]);
                }else{
                    $query->andFilterWhere(['event.idEvent'=>0]);
                }
                
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['event','user','attendant']);

        $dataProvider->sort->attributes['event'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['event.event_name' => SORT_ASC],
        'desc' => ['event.event_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['user'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['user.username' => SORT_ASC],
        'desc' => ['user.username' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['attendant'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['attendant.passport' => SORT_ASC],
        'desc' => ['attendant.passport' => SORT_DESC],
        ];

        // $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idAttendant' => $this->idAttendant,
            'idEvent' => $this->idEvent,
            'registration_date' => $this->registration_date,
            'idUser' => $this->idUser,
            'barcode' => $this->barcode,
            'event_registration.material' => $this->material,
            'break_number' => $this->break_number,
        ]);

        $query->andFilterWhere(['like', 'event.event_name', $this->event])
        ->andFilterWhere(['like', 'user.username', $this->user])
        ->andFilterWhere(['like', 'attendant.passport', $this->attendant]);

        return $dataProvider;
    }
}

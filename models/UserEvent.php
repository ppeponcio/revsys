<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_event".
 *
 * @property int $idUser
 * @property int $idEvent
 * @property int $idUserRegistry
 * @property string $dateTime
 *
 * @property Event $event
 * @property User $userRegistry
 * @property User $user
 */
class UserEvent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idUser', 'idEvent', 'idUserRegistry', 'dateTime'], 'required'],
            [['idUser', 'idEvent', 'idUserRegistry'], 'integer'],
            [['dateTime'], 'safe'],
            [['idUser', 'idEvent'], 'unique', 'targetAttribute' => ['idUser', 'idEvent']],
            [['idEvent'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['idEvent' => 'idEvent']],
            [['idUserRegistry'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['idUserRegistry' => 'id']],
            [['idUser'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['idUser' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idUser' => 'Id User',
            'idEvent' => 'Id Event',
            'idUserRegistry' => 'Id User Registry',
            'dateTime' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['idEvent' => 'idEvent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRegistry()
    {
        return $this->hasOne(User::className(), ['id' => 'idUserRegistry']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'idUser']);
    }
}

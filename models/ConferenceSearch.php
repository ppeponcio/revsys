<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Conference;
use app\models\UserEvent;
use yii\helpers\ArrayHelper;

/**
 * ConferenceSearch represents the model behind the search form of `app\models\Conference`.
 */
class ConferenceSearch extends Conference
{
    /**
     * {@inheritdoc}
     */
    public $event;
    public function rules()
    {
        return [
            [['idConference', 'idEvent'], 'integer'],
            [['conference_name'], 'safe'],
            [['event'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Conference::find();

        if(!Yii::$app->user->can('Ver Todos')){
            if(Yii::$app->user->can('Ver en Evento')){
                $time = new \DateTime('now');
                $today = $time->format('Y-m-d h:i:s');
                $total = UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->count();
                //$query->andFilterWhere(['<=', 'start_date', $today]);  
                $query->andFilterWhere(['>=', 'end_date', $today]);  
                if($total!=0){
                    $events = UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->all();
                    $ids = ArrayHelper::getColumn($events, 'idEvent');
                    $query->andFilterWhere(['event.idEvent'=>$ids]);
                }else{
                  $query->andFilterWhere(['event.idEvent'=>0]);                
                }
            }else{
             $query->andFilterWhere(['event.idEvent'=>0]);   
            }
        }

        $query->joinWith(['event','event.userEvents']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['event'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['event.event_name' => SORT_ASC],
        'desc' => ['event.event_name' => SORT_DESC],
        ];

        // $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idConference' => $this->idConference,
            'idEvent' => $this->idEvent,
        ]);

        $query->andFilterWhere(['like', 'conference_name', $this->conference_name])
        ->andFilterWhere(['like', 'event.event_name', $this->event]);

        return $dataProvider;
    }
}

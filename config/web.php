<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'revsys',
    'name' => 'RevSys',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module',
            // other module settings
        ],
        'rbac' => [
            'class' => 'yii2mod\rbac\Module',
            // Some controller property maybe need to change.
            'controllerMap' => [
                'assignment' => [
                    'class' => 'yii2mod\rbac\controllers\AssignmentController',
                    'userIdentityClass' => 'app\models\User',
                    'searchClass' => [
                        'class' => 'yii2mod\rbac\models\search\AssignmentSearch',
                        'pageSize' => 10,
                    ],
                    'idField' => 'id',
                    'usernameField' => 'username',
                    'gridViewColumns' => [
                         'id',
                         'username',
                         'email'
                     ]
                ],
                'role' => [
                    'class' => 'yii2mod\rbac\controllers\RoleController',
                    'searchClass' => [
                        'class' => 'yii2mod\rbac\models\search\AuthItemSearch',
                        'pageSize' => 10,
                    ],
                ],
                'rule' => [
                    'class' => 'yii2mod\rbac\controllers\RuleController',
                    'searchClass' => [
                        'class' => 'yii2mod\rbac\models\search\BizRuleSearch',
                        'pageSize' => 10
                    ],
                ],
                'route' => [
                    'class' => 'yii2mod\rbac\controllers\RouteController',
                    // for example: exclude `api, debug and gii` modules from list of routes
                    'modelClass' => [
                        'class' => 'yii2mod\rbac\models\RouteModel',
                        'excludeModules' => ['api', 'debug', 'gii'],
                    ],
                ],
            ]
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '2ZDRC8uzjKZBQdH6k7dTrOhEY5DaIm3w',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        //'main' => 'main.php',
                    ],
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest', 'user'],
            // uncomment if you want to cache RBAC items hierarchy
            // 'cache' => 'cache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                // ...
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;

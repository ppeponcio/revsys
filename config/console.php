<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module',
            // other module settings
        ],
        'rbac' => [
            'class' => 'yii2mod\rbac\Module',
            // Some controller property maybe need to change.
            'controllerMap' => [
                'assignment' => [
                    'class' => 'yii2mod\rbac\controllers\AssignmentController',
                    'userIdentityClass' => 'app\models\User',
                    'searchClass' => [
                        'class' => 'yii2mod\rbac\models\search\AssignmentSearch',
                        'pageSize' => 10,
                    ],
                    'idField' => 'id',
                    'usernameField' => 'username',
                    'gridViewColumns' => [
                         'id',
                         'username',
                         'email'
                     ]
                ],
                'role' => [
                    'class' => 'yii2mod\rbac\controllers\RoleController',
                    'searchClass' => [
                        'class' => 'yii2mod\rbac\models\search\AuthItemSearch',
                        'pageSize' => 10,
                    ],
                ],
                'rule' => [
                    'class' => 'yii2mod\rbac\controllers\RuleController',
                    'searchClass' => [
                        'class' => 'yii2mod\rbac\models\search\BizRuleSearch',
                        'pageSize' => 10
                    ],
                ],
                'route' => [
                    'class' => 'yii2mod\rbac\controllers\RouteController',
                    // for example: exclude `api, debug and gii` modules from list of routes
                    'modelClass' => [
                        'class' => 'yii2mod\rbac\models\RouteModel',
                        'excludeModules' => ['api', 'debug', 'gii'],
                    ],
                ],
            ]
        ],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                // ...
            ],
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest', 'user'],
        'db' => $db,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;

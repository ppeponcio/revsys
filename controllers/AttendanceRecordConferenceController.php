<?php

namespace app\controllers;

use Yii;
use app\models\AttendanceRecordConference;
use app\models\AttendanceRecordConferenceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii2mod\rbac\filters\AccessControl;
use app\models\Conference;
use app\models\EventRegistration;
use app\models\UserEvent;

/**
 * AttendanceRecordConferenceController implements the CRUD actions for AttendanceRecordConference model.
 */
class AttendanceRecordConferenceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'allowActions' => [
                    // 'index',
                    // // The actions listed here will be allowed to everyone including guests.
                ]
            ],
        ];
    }

    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::class,
    //             'rules' => [
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['index'],
    //                     'roles' => ['/attendance-record-conference/*'],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['view'],
    //                     'roles' => ['viewARC'],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['create'],
    //                     'roles' => ['createARC'],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['update'],
    //                     'roles' => ['updateARC'],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['delete'],
    //                     'roles' => ['deleteARC'],
    //                 ],
    //             ],
    //         ],
    //     ];
    // }
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Lists all AttendanceRecordConference models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttendanceRecordConferenceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AttendanceRecordConference model.
     * @param integer $idAttendant
     * @param integer $idConference
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idAttendant, $idConference)
    {
        $model = $this->findModel($idAttendant, $idConference);
        if((UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->andWhere(['idEvent'=>$model->event->idEvent])->one() && Yii::$app->user->can('Ver en Evento'))|| Yii::$app->user->can('Ver Todos')){  
            return $this->render('view', [
                'model' => $model,
            ]);
        }else{
            return $this->redirect(['index']);
        }
        
    }

    /**
     * Creates a new AttendanceRecordConference model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttendanceRecordConference();
        $model->idUser = Yii::$app->user->id;
        $model->registration_date = date('Y-m-d h:i:s');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idAttendant' => $model->idAttendant, 'idConference' => $model->idConference]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AttendanceRecordConference model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idAttendant
     * @param integer $idConference
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idAttendant, $idConference)
    {
        $model = $this->findModel($idAttendant, $idConference);
        $model->idUser = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idAttendant' => $model->idAttendant, 'idConference' => $model->idConference]);
        }

        if((UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->andWhere(['idEvent'=>$model->event->idEvent])->one() && Yii::$app->user->can('Ver en Evento'))|| Yii::$app->user->can('Ver Todos')){  
            return $this->render('update', [
                'model' => $model,
            ]);
        }else{
            return $this->redirect(['index']);
        }

        
    }

    /**
     * Deletes an existing AttendanceRecordConference model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idAttendant
     * @param integer $idConference
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idAttendant, $idConference)
    {
        $this->findModel($idAttendant, $idConference)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AttendanceRecordConference model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idAttendant
     * @param integer $idConference
     * @return AttendanceRecordConference the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idAttendant, $idConference)
    {
        if (($model = AttendanceRecordConference::findOne(['idAttendant' => $idAttendant, 'idConference' => $idConference])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionConference() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $idEvent = end($_POST['depdrop_parents']);
            $list = Conference::find()->andWhere(['idEvent' => $idEvent])->asArray()->all();
            $selected  = null;
            if ($idEvent != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $conference) {
                    $out[] = ['id' => $conference['idConference'], 'name' => $conference['conference_name']];
                }

                echo Json::encode(['output' => $out, 'selected'=>$selected]);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected'=>'']);
    }

    public function actionAttendants() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $idEvent = end($_POST['depdrop_parents']);
            $list = EventRegistration::find()->andWhere(['idEvent' => $idEvent])->asArray()->all();
            $selected  = null;
            if ($idEvent != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $attedant) {
                    $out[] = ['id' => $attedant['idAttendant'], 'name' => $attedant['barcode']];
                }

                echo Json::encode(['output' => $out, 'selected'=>$selected]);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected'=>'']);
    }
}

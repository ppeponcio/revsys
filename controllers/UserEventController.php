<?php

namespace app\controllers;

use Yii;
use app\models\UserEvent;
use app\models\UserEventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii2mod\rbac\filters\AccessControl;

/**
 * UserEventController implements the CRUD actions for UserEvent model.
 */
class UserEventController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'allowActions' => [
                    // 'index',
                    // // The actions listed here will be allowed to everyone including guests.
                ]
            ],
        ];
    }
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Lists all UserEvent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserEventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserEvent model.
     * @param integer $idUser
     * @param integer $idEvent
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idUser, $idEvent)
    {
        $model = $this->findModel($idUser, $idEvent);
        if(Yii::$app->user->can('Ver Todos') || (Yii::$app->user->can('Ver en Evento') && UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->andWhere(['idEvent'=>$model->event->idEvent])->one())){
            return $this->render('view', [
            'model' => $model,
            ]);
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new UserEvent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserEvent();
        $model->idUserRegistry = Yii::$app->user->id;
        $model->dateTime = date('Y-m-d h:i:s');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idUser' => $model->idUser, 'idEvent' => $model->idEvent]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UserEvent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idUser
     * @param integer $idEvent
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idUser, $idEvent)
    {
        $model = $this->findModel($idUser, $idEvent);
        $model->idUserRegistry = Yii::$app->user->id;
        $model->dateTime = date('Y-m-d h:i:s');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idUser' => $model->idUser, 'idEvent' => $model->idEvent]);
        }

        if(Yii::$app->user->can('Ver Todos') || (Yii::$app->user->can('Ver en Evento') && UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->andWhere(['idEvent'=>$model->event->idEvent])->one())){
            return $this->render('update', [
            'model' => $model,
            ]);
        }else{
            return $this->redirect(['index']);
        }

    }

    /**
     * Deletes an existing UserEvent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idUser
     * @param integer $idEvent
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idUser, $idEvent)
    {
        $this->findModel($idUser, $idEvent)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserEvent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idUser
     * @param integer $idEvent
     * @return UserEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idUser, $idEvent)
    {
        if (($model = UserEvent::findOne(['idUser' => $idUser, 'idEvent' => $idEvent])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace app\controllers;

use Yii;
use app\models\EventRegistration;
use app\models\EventRegistrationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii2mod\rbac\filters\AccessControl;
use app\models\UserEvent;
/**
 * EventRegistrationController implements the CRUD actions for EventRegistration model.
 */
class EventRegistrationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'allowActions' => [
                    // 'index',
                    // // The actions listed here will be allowed to everyone including guests.
                ]
            ],
        ];
    }
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Lists all EventRegistration models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventRegistrationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EventRegistration model.
     * @param integer $idAttendant
     * @param integer $idEvent
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idAttendant, $idEvent)
    {
        $model = $this->findModel($idAttendant, $idEvent);
        if(Yii::$app->user->can('Ver Todos') || (Yii::$app->user->can('Ver en Evento') && UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->andWhere(['idEvent'=>$model->event->idEvent])->one())){
            return $this->render('view', [
                'model' => $model,
            ]);
        }else{
            return $this->redirect(['index']);
        }
        
    }

    /**
     * Creates a new EventRegistration model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EventRegistration();
        $model->idUser = Yii::$app->user->id;
        $model->registration_date = date('Y-m-d h:i:s');
        $model->break_number = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idAttendant' => $model->idAttendant, 'idEvent' => $model->idEvent]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EventRegistration model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idAttendant
     * @param integer $idEvent
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idAttendant, $idEvent)
    {
        $model = $this->findModel($idAttendant, $idEvent);
        $model->idUser = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idAttendant' => $model->idAttendant, 'idEvent' => $model->idEvent]);
        }

        //$model = $this->findModel($idAttendant, $idEvent);
        if(Yii::$app->user->can('Ver Todos') || (Yii::$app->user->can('Ver en Evento') && UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->andWhere(['idEvent'=>$model->event->idEvent])->one())){
            return $this->render('update', [
            'model' => $model,
            ]);
        }else{
            return $this->redirect(['index']);
        }

        
    }

    /**
     * Deletes an existing EventRegistration model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idAttendant
     * @param integer $idEvent
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idAttendant, $idEvent)
    {
        $this->findModel($idAttendant, $idEvent)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EventRegistration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idAttendant
     * @param integer $idEvent
     * @return EventRegistration the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idAttendant, $idEvent)
    {
        if (($model = EventRegistration::findOne(['idAttendant' => $idAttendant, 'idEvent' => $idEvent])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionBreak()
    {
        $model = new EventRegistration();
        //$model = new EventRegistration(['scenario' => 'break']);


        if ($model->load(Yii::$app->request->post())) {
            $model = $this->findModel($model->idAttendant, $model->idEvent);
            $model->break_number++;
            if ($model->validate()) {
                $model->update();
                return $this->redirect(['break']);
            }
          
            
        }

        return $this->render('break', [
            'model' => $model,
        ]);
    }

    public function actionSample() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $idEvent = end($_POST['depdrop_parents']);
            $list = EventRegistration::find()->andWhere(['idEvent' => $idEvent])->asArray()->all();
            $selected  = null;
            if ($idEvent != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $registration) {
                    $out[] = ['id' => $registration['idAttendant'], 'name' => $registration['barcode']];
                }

                echo Json::encode(['output' => $out, 'selected'=>$selected]);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected'=>'']);
    }

}

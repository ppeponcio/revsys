<?php

namespace app\controllers;

use Yii;
use app\models\Event;
use app\models\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii2mod\rbac\filters\AccessControl;
use yii\web\UploadedFile;
use app\models\UserEvent;
/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'allowActions' => [
                    // 'index',
                    // // The actions listed here will be allowed to everyone including guests.
                ]
            ],
        ];
    }
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if(Yii::$app->user->can('Ver Todos') || (Yii::$app->user->can('Ver en Evento') && UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->andWhere(['idEvent'=>$model->idEvent])->one())){
            return $this->render('view', [
                'model' => $model,
            ]);
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // $model = new Event();
        // $model->break_quantity =0;
        // $model->banner = UploadedFile::getInstance($model, 'banner');
        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->idEvent]);
        // }

        // return $this->render('create', [
        //     'model' => $model,
        // ]);

    $model = new Event();
    if ($model->load(Yii::$app->request->post())) {
        $model->banner = UploadedFile::getInstance($model, 'banner');

        if ($model->validate()) {
            if ($model->banner) {
                $filePath = 'uploads/' . $model->banner->baseName . '.' . $model->banner->extension;
                if ($model->banner->saveAs($filePath)) {
                    $model->banner_route = '/'.$filePath;
                }
            }

            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->idEvent]);
            }
        }
    }

    return $this->render('create', [
        'model' => $model,
    ]);

    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        // $model = $this->findModel($id);
        // $model->banner = UploadedFile::getInstance($model, 'banner');
        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->idEvent]);
        // }

        // return $this->render('update', [
        //     'model' => $model,
        // ]);
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
        $model->banner = UploadedFile::getInstance($model, 'banner');

        if ($model->validate()) {
                if ($model->banner) {
                    $filePath = 'uploads/' . $model->banner->baseName . '.' . $model->banner->extension;
                    if ($model->banner->saveAs($filePath)) {
                        $model->banner_route = '/'.$filePath;
                    }
                }

                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->idEvent]);
                }
            }
        }

        if(Yii::$app->user->can('Ver Todos') || (Yii::$app->user->can('Ver en Evento') && UserEvent::find()->where(['idUser'=>Yii::$app->user->id])->andWhere(['idEvent'=>$model->idEvent])->one())){
            return $this->render('update', [
                'model' => $model,
            ]);
        }else{
            return $this->redirect(['index']);
        }

        

    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}

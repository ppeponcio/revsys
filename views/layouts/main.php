<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            //['label' => 'Home', 'url' => ['/site/index']],
            //['label' => 'About', 'url' => ['/site/about']],
            //['label' => 'Contact', 'url' => ['/site/contact']],
            //['label' => 'RBAC', 'url' => ['/rbac']],
            // ['label' => 'Users', 'items' => [
            //         ['label' => 'Index', 'url' => ['/user/index']],
            //         ['label' => 'Create', 'url' => ['/user/create']],
            //         ['label' => 'RBAC', 'items' => [
            //                 // ['label' => 'Index', 'url' => ['/rbac']],
            //                 ['label' => 'Route', 'url' => ['/rbac/route']],
            //                 ['label' => 'Permission', 'url' => ['/rbac/permission']],
            //                 ['label' => 'Role', 'url' => ['/rbac/role']],
            //                 ['label' => 'Assignment', 'url' => ['/rbac/assignment']],
            //             ],
            //         ],
            //     ],
            // ],
            //['label' => 'Events', 'url' => ['/event/index']],

            // Yii::$app->user->can('/event/create') || Yii::$app->user->can('/event/index') ? (
            //     ['label' => 'Events', 'items' => [
            //         ['label' => 'Index', 'url' => ['/event/index']],
            //         ['label' => 'Create', 'url' => ['/event/create']],
            //     ],
            // ]
            // ) :
            //['label' => 'Events', 'url' => ['/event/index'], Yii::$app->user->can('/event/index') ? (['visible' =>'true']) : (['visible' =>'false'])],
            // ['label' => 'RBAC', 'items' => 
            //     [
            //         ['label' => 'Route', 'url' => ['/rbac/route']],
            //         ['label' => 'Permission', 'url' => ['/rbac/permission']],
            //         ['label' => 'Role', 'url' => ['/rbac/role']],
            //         ['label' => 'Assignment', 'url' => ['/rbac/assignment']],
            //     ],
            // ],


            Yii::$app->user->can('/user/index') ? (
                ['label' => 'Usuarios', 'url' => ['/user/index'],'visible' =>true]
            ) : (
                ['label' => 'Usuarios', 'url' => ['/user/index'],'visible' =>false]
            ),

            Yii::$app->user->can('/rbac/route/index') || Yii::$app->user->can('/rbac/permission/index') || Yii::$app->user->can('/rbac/role/index') || Yii::$app->user->can('/rbac/assignment/index') ? (
                ['label' => 'RBAC', 'items' => 
                    [
                        Yii::$app->user->can('/rbac/route/index') ? (
                            ['label' => 'Rutas', 'url' => ['/rbac/route/index'],'visible' =>true]
                        ) : (
                            ['label' => 'Rutas', 'url' => ['/rbac/route/index'],'visible' =>false]
                        ),
                        Yii::$app->user->can('/rbac/permission/index') ? (
                            ['label' => 'Permisos', 'url' => ['/rbac/permission/index'],'visible' =>true]
                        ) : (
                            ['label' => 'Permisos', 'url' => ['/rbac/permission/index'],'visible' =>false]
                        ),
                        Yii::$app->user->can('/rbac/role/index') ? (
                            ['label' => 'Roles', 'url' => ['/rbac/role/index'],'visible' =>true]
                        ) : (
                            ['label' => 'Roles', 'url' => ['/rbac/role/index'],'visible' =>false]
                        ),
                        Yii::$app->user->can('/rbac/assignment/index') ? (
                            ['label' => 'Asignacion', 'url' => ['/rbac/assignment/index'],'visible' =>true]
                        ) : (
                            ['label' => 'Asignacion', 'url' => ['/rbac/assignment/index'],'visible' =>false]
                        ),
                    ],
                ]
            ) : (
                ['label' => 'RBAC', 'url' => ['/rbac'],'visible' =>false]
            ),

            Yii::$app->user->can('/carrera/index') ? (
                ['label' => 'Carreras', 'url' => ['/carrera/index'],'visible' =>true]
            ) : (
                ['label' => 'Carreras', 'url' => ['/carrera/index'],'visible' =>false]
            ),

            Yii::$app->user->can('/event/index') ? (
                ['label' => 'Eventos', 'url' => ['/event/index'],'visible' =>true]
            ) : (
                ['label' => 'Eventos', 'url' => ['/event/index'],'visible' =>false]
            ),

            
            Yii::$app->user->can('/user-event/index') ? (
                ['label' => 'Usuarios - Evento', 'url' => ['/user-event/index'],'visible' =>true]
            ) : (
                ['label' => 'Usuarios - Evento', 'url' => ['/user-event/index'],'visible' =>false]
            ),

            Yii::$app->user->can('/conference/index') ? (
                ['label' => 'Conferencias', 'url' => ['/conference/index'],'visible' =>true]
            ) : (
                ['label' => 'Conferencias', 'url' => ['/conference/index'],'visible' =>false]
            ),

            Yii::$app->user->can('/attendant/index') ? (
                ['label' => 'Personas', 'url' => ['/attendant/index'],'visible' =>true]
            ) : (
                ['label' => 'Personas', 'url' => ['/attendant/index'],'visible' =>false]
            ),

            Yii::$app->user->can('/event-registration/index') ? (
                ['label' => 'Registro a Eventos', 'url' => ['/event-registration/index'],'visible' =>true]
            ) : (
                ['label' => 'Registro a Eventos', 'url' => ['/event-registration/index'],'visible' =>false]
            ),

            Yii::$app->user->can('/attendance-record-conference/index') ? (
                ['label' => 'Asistencia', 'url' => ['/attendance-record-conference/index'],'visible' =>true]
            ) : (
                ['label' => 'Asistencia', 'url' => ['/attendance-record-conference/index'],'visible' =>false]
            ),

            Yii::$app->user->can('/event-registration/break') ? (
                ['label' => 'Breaks', 'url' => ['/event-registration/break'],'visible' =>true]
            ) : (
                ['label' => 'Breaks', 'url' => ['/event-registration/break'],'visible' =>false]
            ),


            // Yii::$app->user->can('/translatemanager/language/list') ? (
            //     ['label' => 'Language', 
            //         'items' => [
            //             ['label' => 'List', 'url' => ['/translatemanager/language/list']],
            //             ['label' => 'Create', 'url' => ['/translatemanager/language/create']],
            //             ['label' => 'Scan', 'url' => ['/translatemanager/language/scan']],
            //             ['label' => 'Optimizer', 'url' => ['/translatemanager/language/optimizer']],
            //         ],     
            //     ]
            // ) :

            //['label' => 'Carrers', 'url' => ['/carrera/index']],
            // ['label' => 'Carrers', 'items' => [
            //         ['label' => 'Index', 'url' => ['/carrera/index']],
            //         ['label' => 'Create', 'url' => ['/carrera/create']],
            //     ],
            // ],
            //['label' => 'Conference', 'url' => ['/conference/index']],
            // ['label' => 'Conference', 'items' => [
            //         ['label' => 'Index', 'url' => ['/conference/index']],
            //         ['label' => 'Create', 'url' => ['/conference/create']],
            //     ],
            // ],
            //['label' => 'Attendants', 'url' => ['/attendant/index']],
            // ['label' => 'Attendants', 'items' => [
            //         ['label' => 'Index', 'url' => ['/attendant/index']],
            //         ['label' => 'Create', 'url' => ['/attendant/create']],
            //     ],
            // ],
            //['label' => 'Event - Users', 'url' => ['/user-event/index']],
            // ['label' => 'Event - Users', 'items' => [
            //         ['label' => 'Index', 'url' => ['/user-event/index']],
            //         ['label' => 'Create', 'url' => ['/user-event/create']],
            //     ],
            // ],
            //['label' => 'Event Registration', 'url' => ['/event-registration/index']],
            // ['label' => 'Registration', 'items' => [
            //         ['label' => 'Index', 'url' => ['/event-registration/index']],
            //         ['label' => 'Create', 'url' => ['/event-registration/create']],
            //         ['label' => 'Breaks', 'url' => ['/event-registration/break']],
            //     ],
            // ],
            //['label' => 'Attendance', 'url' => ['/attendance-record-conference/index']],
            // ['label' => 'Attendance', 'items' => [
            //         ['label' => 'Index', 'url' => ['/attendance-record-conference/index']],
            //         ['label' => 'Create', 'url' => ['/attendance-record-conference/create']],
            //     ],
            // ],
            // ['label' => 'Breaks', 'url' => ['/event-registration/break']],
            // ['label' => 'Language', 
            //     'items' => [
            //         ['label' => 'List', 'url' => ['/translatemanager/language/list']],
            //         ['label' => 'Create', 'url' => ['/translatemanager/language/create']],
            //         ['label' => 'Scan', 'url' => ['/translatemanager/language/scan']],
            //         ['label' => 'Optimizer', 'url' => ['/translatemanager/language/optimizer']],
            //     ],     
            // ],
            //['label' => 'Language', 'url' => ['/translatemanager/language/list']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-right">&copy; UDLA <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

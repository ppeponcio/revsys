<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\Modal;
//use kartikorm\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'event_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
    'options' => ['placeholder' => 'Enter event start date and time'],
    'pluginOptions' => [
        'autoclose' => true
    ]
]);?>

    <?= $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
    'options' => ['placeholder' => 'Enter event end date and time'],
    'pluginOptions' => [
        'autoclose' => true
    ]
]);?>

    <?= $form->field($model, 'material')->checkbox() ?>

    <?= $form->field($model, 'breaks')->checkbox() ?>

    <?= $form->field($model, 'break_quantity')->textInput() ?>

    <?= $form->field($model, 'banner')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

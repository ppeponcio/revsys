<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Event */

$this->title = $model->idEvent;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idEvent], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idEvent], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguró?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idEvent',
            'event_name',
            'start_date',
            'end_date',
            //'banner_route',
            [
                'attribute'=>'banner_route',
                'value'=>$model->banner_route,
                'format' => ['image'],
                'visible' => $model->banner_route,
            ],
            [
             'attribute' => 'Total Registros',
             'value' => $model->getTotalAttendants(),
             ],
            'breaks:boolean',
            [
             'attribute' => 'break_quantity',
             'value' => $model->break_quantity,
             'visible' => $model->breaks,
             ],
            [
             'attribute' => 'Total Breaks Entregados',
             'value' => $model->getTotalBreaks(),
             'visible' => $model->breaks,
             ],
             [
             'attribute' => 'Total Breaks Sobrantes',
             'value' => $model->getBreaksOverplus(),
             'visible' => $model->breaks,
             ],
             [
             'attribute' => 'Total Personas con todos los Break',
             'value' => $model->getTotalAttedantsBreaksFull(),
             'visible' => $model->breaks,
             ],
             [
             'attribute' => 'Total Personas con algunos Break',
             'value' => $model->getTotalAttedantsBreaks(),
             'visible' => $model->breaks,
             ],
             [
             'attribute' => 'Total Personas con 0 Break',
             'value' => $model->getTotalAttedantsBreaksZero(),
             'visible' => $model->breaks,
             ],
             'material:boolean',
             [
             'attribute' => 'Total Material Entregado',
             'value' => $model->getTotalMaterial(),
             'visible' => $model->material,
             ],
             [
             'attribute' => 'Total Material Sin Entregar',
             'value' => $model->getMissingMaterial(),
             'visible' => $model->material,
             ],
        ],
    ]) ?>

</div>

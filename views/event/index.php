<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Eventos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Evento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= 
    Yii::$app->user->can('Generar Reportes') ? (
    ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'noExportColumns' => [],
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'idEvent',
                'event_name',
                'start_date',
                'end_date',
                [
                 'attribute' => 'Total Registros',
                 'value' => function($model) {
                    return $model->getTotalAttendants();
                    },
                 ],
                'breaks:boolean',
                [
                 'attribute' => 'break_quantity',
                 'value' => function($model) {
                        return $model->break_quantity;
                    },
                 'visible' => function($model) {
                    return $model->breaks;
                    },
                 
                 ],
                [
                 'attribute' => 'Total Breaks Entregados',
                 'value' => function($model) {
                    return $model->getTotalBreaks();
                    },
                 'visible' => function($model) {
                    return $model->breaks;
                    },
                 ],
                 [
                 'attribute' => 'Total Breaks Sobrantes',
                 'value' => function($model) {
                    return $model->getBreaksOverplus();
                    },
                 'visible' => function($model) {
                    return $model->breaks;
                    },
                 ],
                 [
                 'attribute' => 'Total Personas con todos los Break',
                 'value' => function($model) {
                    return $model->getTotalAttedantsBreaksFull();
                    },
                 'visible' => function($model) {
                    return $model->breaks;
                    },
                 ],
                 [
                 'attribute' => 'Total Personas con algunos Break',
                 'value' => function($model) {
                    return $model->getTotalAttedantsBreaks();
                    },
                 'visible' => function($model) {
                    return $model->breaks;
                    },
                 ],
                 [
                 'attribute' => 'Total Personas con 0 Break',
                 'value' => function($model) {
                    return $model->getTotalAttedantsBreaksZero();
                    },
                 'visible' => function($model) {
                    return $model->breaks;
                    },
                 ],
                 'material:boolean',
                 [
                 'attribute' => 'Total Material Entregado',
                 'value' => function($model) {
                    return $model->getTotalMaterial();
                    },
                 'visible' => function($model) {
                    return $model->material;
                    },
                 ],
                 [
                 'attribute' => 'Total Material Sin Entregar',
                 'value' => function($model) {
                return  $model->getMissingMaterial();
                },
                 'visible' => function($model) {
                    return $model->material;
                    },
                 ],
                
                //'conferences',
                //'break_quantity',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ])
    ) : (
        ''
        )
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idEvent',
            'event_name',
            'start_date',
            'end_date',
            'breaks:boolean',
            'material:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

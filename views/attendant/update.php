<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Attendant */

$this->title = 'Update Attendant: ' . $model->idAttendant;
$this->params['breadcrumbs'][] = ['label' => 'Attendants', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idAttendant, 'url' => ['view', 'id' => $model->idAttendant]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="attendant-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

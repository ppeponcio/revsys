<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Attendant */

$this->title = 'Create Attendant';
$this->params['breadcrumbs'][] = ['label' => 'Attendants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

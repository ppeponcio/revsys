<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Attendant */

$this->title = $model->idAttendant;
$this->params['breadcrumbs'][] = ['label' => 'Attendants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendant-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idAttendant], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idAttendant], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idAttendant',
            'first_name',
            'last_name',
            'passport',
            'carrera.carrer_name',

        ],
    ]) ?>

</div>

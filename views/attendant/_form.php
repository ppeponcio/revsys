<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Carrera;
/* @var $this yii\web\View */
/* @var $model app\models\Attendant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendant-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'passport')->textInput(['maxlength' => true]) ?>

    <?php
    $carreras=Carrera::find()->all();

	$data=ArrayHelper::map($carreras,'idCarrera','carrer_name');

    echo $form->field($model, 'idCarrera')->widget(Select2::classname(), 
    [
	    'data' => $data,
	    'options' => ['placeholder' => 'Select a Carrer'],
	    'pluginOptions' => [
	    'allowClear' => true
   	 ],
	])->label('Carrera'); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

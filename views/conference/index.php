<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConferenceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conferencias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conference-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear una conferencia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= 
    Yii::$app->user->can('Generar Reportes') ? (
    ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'noExportColumns' => [],
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'idConference',
                'conference_name',
                //'event.event_name',
                [
                 'attribute' => 'event',
                 'label'=>'Evento',
                 'value' => 'event.event_name'
                 ],
                 'hora_inicio',
                 'hora_fin',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ])
    ) : (
        ''
        )
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idConference',
            'conference_name',
            //'event.event_name',
            [
             'attribute' => 'event',
             'label'=>'Evento',
             'value' => 'event.event_name'
             ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conference */

$this->title = 'Update Conference: ' . $model->idConference;
$this->params['breadcrumbs'][] = ['label' => 'Conferences', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idConference, 'url' => ['view', 'id' => $model->idConference]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conference-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Event;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Conference */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conference-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'conference_name')->textInput(['maxlength' => true]) ?>

    <?php

    $time = new \DateTime('now');
    $today = $time->format('Y-m-d h:i:s');
    

    if(Yii::$app->user->can('Ver Todos')){
        $events=Event::find()->where(['>','end_date',$today])->all();
    }else{
        //$events=Event::find()->innerJoin()->where(['>','end_date',$today])->all();
        $events=Event::find()->joinWith(["userEvents eu"],true,"INNER JOIN")->andWhere(['>=', 'end_date', $today])->andWhere(["eu.idUser"=>Yii::$app->user->id])->all();
    }

	$data=ArrayHelper::map($events,'idEvent','event_name');

    echo $form->field($model, 'idEvent')->widget(Select2::classname(), 
    [
	    'data' => $data,
	    'options' => ['placeholder' => 'Seleccione un evento'],
	    'pluginOptions' => [
	    'allowClear' => true
   	 ],
])->label('Event');

        ?>

    <?= $form->field($model, 'hora_inicio')->widget(DateTimePicker::classname(), [
    'options' => ['placeholder' => 'Ingrese hora de inicio'],
    'pluginOptions' => [
        'autoclose' => true
    ]
]);?>

    <?= $form->field($model, 'hora_fin')->widget(DateTimePicker::classname(), [
    'options' => ['placeholder' => 'Ingrese hora de finalización'],
    'pluginOptions' => [
        'autoclose' => true
    ]
]);?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

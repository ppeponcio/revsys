<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Conference;
use app\models\EventRegistration;
use app\models\Event;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\AttendanceRecordConference */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendance-record-conference-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $time = new \DateTime('now');
    $today = $time->format('Y-m-d h:i:s');
    $events=Event::find()->joinWith(["userEvents eu"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(["eu.idUser"=>Yii::$app->user->id])->all();

    $listData=ArrayHelper::map($events,'idEvent','event_name');


     echo $form->field($model, 'event')->dropDownList($listData, ['id'=>'idEvent'])->label('Event');

    ?>

    <?php
     echo $form->field($model, 'idConference')->widget(DepDrop::classname(), [
        'type'=>DepDrop::TYPE_SELECT2,
        'options'=>['id'=>'idConference', 'class'=>'form-control'],
        //'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
        'pluginOptions'=>[
            'depends'=>['idEvent'],
            'placeholder'=>'Select a Conference',
            'url'=>Url::to(['/attendance-record-conference/conference']),
        'initialize'=>true,
    ]])->label('Conference');
    ?>

    <?php
     echo $form->field($model, 'idAttendant')->widget(DepDrop::classname(), [
        'type'=>DepDrop::TYPE_SELECT2,
        'options'=>['id'=>'idAttendant', 'class'=>'form-control'],
        //'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
        'pluginOptions'=>[
            'depends'=>['idEvent'],
            'placeholder'=>'Select an Attendant',
            'url'=>Url::to(['/attendance-record-conference/attendants']),
        'initialize'=>true,
    ]])->label('Attendant');
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AttendanceRecordConferenceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asistencia';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-record-conference-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Asistencia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= 
    Yii::$app->user->can('Generar Reportes') ? (
    ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'noExportColumns' => [],
        'filename' => 'Asistencia',
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'idAttendant',
                [
                 'attribute' => 'event_name',
                 'label' => 'Evento',
                 'value' => 'event.event_name',
                 'visible'=> Yii::$app->user->can('Ver en Evento') || Yii::$app->user->can('Ver Todos'),
                 ],
                [
                 'attribute' => 'attendant',
                 'label' => 'Pasaporte',
                 'value' => 'attendant.passport'
                 ],
                 'attendant.first_name',
                 'attendant.last_name',
                // 'idConference',
                [
                 'attribute' => 'conference',
                 'label' => 'Conferencia',
                 'value' => 'conference.conference_name'
                 ],
                 
                'registration_date',
                // 'idUser',
                [
                 'attribute' => 'user',
                 'value' => 'user.username',
                 'label' => 'Usuario',
                 'visible'=> Yii::$app->user->can('Ver en Evento') || Yii::$app->user->can('Ver Todos'),
                 //'visible'=> ,
                 ],
                 
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ])
    ) : (
        ''
        )
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'idAttendant',
            [
             'attribute' => 'event_name',
             'label' => 'Evento',
             'value' => 'event.event_name',
             'visible'=> Yii::$app->user->can('Ver en Evento') || Yii::$app->user->can('Ver Todos'),
             ],
            [
             'attribute' => 'attendant',
             'label' => 'Pasaporte',
             'value' => 'attendant.passport'
             ],

            // 'idConference',
            [
             'attribute' => 'conference',
             'label' => 'Conferencia',
             'value' => 'conference.conference_name'
             ],
             
            'registration_date',
            // 'idUser',
            [
             'attribute' => 'user',
             'value' => 'user.username',
             'label' => 'Usuario',
             'visible'=> Yii::$app->user->can('Ver en Evento') || Yii::$app->user->can('Ver Todos'),
             //'visible'=> ,
             ],
             
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

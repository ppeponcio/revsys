<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AttendanceRecordConference */

$this->title = $model->idAttendant;
$this->params['breadcrumbs'][] = ['label' => 'Attendance Record Conferences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-record-conference-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idAttendant' => $model->idAttendant, 'idConference' => $model->idConference], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'idAttendant' => $model->idAttendant, 'idConference' => $model->idConference], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'attendant.first_name',
            'attendant.last_name',
            'attendant.passport',
            'conference.conference_name',
            'registration_date',
            'user.username',
            'event.event_name'
        ],
    ]) ?>

</div>

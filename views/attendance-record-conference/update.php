<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AttendanceRecordConference */

$this->title = 'Update Attendance Record Conference: ' . $model->idAttendant;
$this->params['breadcrumbs'][] = ['label' => 'Attendance Record Conferences', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idAttendant, 'url' => ['view', 'idAttendant' => $model->idAttendant, 'idConference' => $model->idConference]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="attendance-record-conference-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

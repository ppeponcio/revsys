<?php
use yii\helpers\Json;
use app\models\Event;
/* @var $this yii\web\View */

$this->title = 'RevSys';
?>
<div class="site-index">

    <?php
    if(Yii::$app->user->isGuest){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d h:i:s'); 
        $list = Event::find()->andWhere(['>=', 'end_date', $today])->asArray()->all();
        $out = [];   
        if (count($list) > 0) {
            $selected = '';
            foreach ($list as $i => $item) {
                $out[] = ['title' => $item['event_name'], 'href' => $item['banner_route'],'type' => 'text/html'];
            }
        }
    }else{
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d h:i:s'); 
        $list = Event::find()->joinWith(["userEvents eu"],true,"INNER JOIN")->andWhere(["eu.idUser"=>Yii::$app->user->id])->andWhere(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->asArray()->all();
        $out = [];   
        if (count($list) > 0) {
            $selected = '';
            foreach ($list as $i => $item) {
                $out[] = ['title' => $item['event_name'], 'href' => $item['banner_route'],'type' => 'text/html'];
            }
        }

    }
    

    ?>
    <?= dosamigos\gallery\Carousel::widget([
        'items' => $out, 'json' => true,
        'clientEvents' => [
            'onslide' => 'function(index, slide) {
                console.log(slide);
            }'
    ]]);?>
</div>

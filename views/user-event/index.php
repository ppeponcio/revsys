<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserEventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Eventos - Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Evento - Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= 
    Yii::$app->user->can('Generar Reportes') ? (
    ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'noExportColumns' => [],
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'idUser',
                [
                 'attribute' => 'user',
                 'label' => 'Usuario',
                 'value' => 'user.username'
                 ],
                [
                 'attribute' => 'event',
                 'label' => 'Evento',
                 'value' => 'event.event_name'
                 ],
                //'idEvent',
                //'idUserRegistry',
                [
                 'attribute' => 'userRegistry',
                 'label' => 'Usuario Registro',
                 'value' => 'userRegistry.username'
                 ],
                'dateTime',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ])
    ) : (
        ''
        )
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idUser',
            [
             'attribute' => 'user',
             'label' => 'Usuario',
             'value' => 'user.username'
             ],
            [
             'attribute' => 'event',
             'label' => 'Evento',
             'value' => 'event.event_name'
             ],
            //'idEvent',
            //'idUserRegistry',
            [
             'attribute' => 'userRegistry',
             'label' => 'Usuario Registro',
             'value' => 'userRegistry.username'
             ],
            'dateTime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

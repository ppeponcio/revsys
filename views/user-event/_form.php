<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\UserEvent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $users=User::find()->all();

	$data=ArrayHelper::map($users,'id','username');

    echo $form->field($model, 'idUser')->widget(Select2::classname(), 
    [
	    'data' => $data,
	    'options' => ['placeholder' => 'Select a user'],
	    'pluginOptions' => [
	    'allowClear' => true
   	 ],
])->label('User');

    ?>



	<?php
    $time = new \DateTime('now');
    $today = $time->format('Y-m-d h:i:s');
    //$events=Event::find()->where(['>','end_date',$today])->all();

    if(Yii::$app->user->can('Ver Todos')){
        $events=Event::find()->where(['>','end_date',$today])->all();
    }else{
        //$events=Event::find()->innerJoin()->where(['>','end_date',$today])->all();
        $events=Event::find()->joinWith(["userEvents eu"],true,"INNER JOIN")->andWhere(['>=', 'end_date', $today])->andWhere(["eu.idUser"=>Yii::$app->user->id])->all();
    }

	$data=ArrayHelper::map($events,'idEvent','event_name');

    echo $form->field($model, 'idEvent')->widget(Select2::classname(), 
    [
	    'data' => $data,
	    'options' => ['placeholder' => 'Select an Event'],
	    'pluginOptions' => [
	    'allowClear' => true
   	 ],])->label('Event');

        ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserEvent */

$this->title = $model->idUser;
$this->params['breadcrumbs'][] = ['label' => 'User Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idUser' => $model->idUser, 'idEvent' => $model->idEvent], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idUser' => $model->idUser, 'idEvent' => $model->idEvent], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user.username',
            'event.event_name',
            [
                'attribute'=>'userRegistry.username',
                'label'=>'Usuario Registro',
            ],
            
            'dateTime',
        ],
    ]) ?>

</div>

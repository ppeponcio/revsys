<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\models\Attendant;

/* @var $this yii\web\View */
/* @var $model app\models\EventRegistration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-registration-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $attendats=Attendant::find()->all();

    $data=ArrayHelper::map($attendats,'idAttendant','passport');

    echo $form->field($model, 'idAttendant')->widget(Select2::classname(), 
    [
        'data' => $data,
        'options' => ['placeholder' => 'Select an Attendant'],
        'pluginOptions' => [
        'allowClear' => true
     ],])->label('Attendant');

        ?>

    <?php
    $time = new \DateTime('now');
    $today = $time->format('Y-m-d h:i:s');
    $id = Yii::$app->user->id;
    $events=Event::find()->joinWith('userEvents')->where(['idUser'=>$id])->andWhere(['>=','end_date',$today])->andWhere(['<','start_date',$today])->all();

    $data=ArrayHelper::map($events,'idEvent','event_name');

    echo $form->field($model, 'idEvent')->widget(Select2::classname(), 
    [
        'data' => $data,
        'options' => ['placeholder' => 'Select an Event'],
        'pluginOptions' => [
        'allowClear' => true
     ],])->label('Event');

        ?>

    <?= $form->field($model, 'barcode')->textInput() ?>

    <?= $form->field($model, 'material')->checkBox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

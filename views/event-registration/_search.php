<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventRegistrationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-registration-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idAttendant') ?>

    <?= $form->field($model, 'idEvent') ?>

    <?= $form->field($model, 'registration_date') ?>

    <?= $form->field($model, 'idUser') ?>

    <?= $form->field($model, 'barcode') ?>

    <?php // echo $form->field($model, 'material') ?>

    <?php // echo $form->field($model, 'break_number') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

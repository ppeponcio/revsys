<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventRegistrationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Registro a Eventos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-registration-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Registro a Evento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= 
    Yii::$app->user->can('Generar Reportes') ? (
    ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'noExportColumns' => [],
        'filename' => 'RegistroEventos',
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'attendant.first_name',
                'attendant.last_name',
                'attendant.passport',
                'event.event_name',
                'registration_date',
                'barcode',
                [
                 'attribute' => 'material',
                 'value' => function($model){return $model->material;},
                 'format' => 'boolean',
                 'visible' => function($model){return $model->event->material;},
                 ],
                [
                 'attribute' => 'break_number',
                 'value' => function($model){return $model->break_number;},
                 'visible' => function($model){return $model->event->breaks;},
                 ],
                 [
                    'attribute' => 'Total Attended Conferences',
                    'value' => function($model){return count($model->attendant->conferences);},
                 ],
                 [
                    'attribute' => 'Attended Conferences',
                    'value' => function($model){return implode(', ',\yii\helpers\ArrayHelper::map($model->attendant->conferences, 'idConference', 'conference_name'));},
                 ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
        ])
    ) : (
        ''
        )
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
             'attribute' => 'attendant',
             'label'=>'Pasaporte',
             'value' => 'attendant.passport'
             ],
            [
             'attribute' => 'event',
             'label'=>'Evento',
             'value' => 'event.event_name'
             ],
            'registration_date',
            [
             'attribute' => 'user',
             'value' => 'user.username',
             'label'=>'Usuario',
             'visible' =>Yii::$app->user->can('Ver en Evento') || Yii::$app->user->can('Ver Todos'),
             ],
            'barcode',
            Yii::$app->user->can('Generar Reportes') ? (
                [
                    'attribute'=>'material',
                    'format' =>'boolean',
                    'visible' =>'true',
                ]
            ) : (
                [
                    'attribute'=>'material',
                    'format' =>'boolean',
                    'visible' =>false,
                ]
        ),
            //'break_number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

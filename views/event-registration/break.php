<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\EventRegistration;
use app\models\Event;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\EventRegistration */
/* @var $form ActiveForm */
?>
<div class="event-registration-break">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $time = new \DateTime('now');
    $today = $time->format('Y-m-d h:i:s');
    $events=Event::find()->joinWith(["userEvents eu"],true,"INNER JOIN")->where(['<=', 'start_date', $today])->andWhere(['>=', 'end_date', $today])->andWhere(['=', 'breaks','1'])->andWhere(["eu.idUser"=>Yii::$app->user->id])->all();

    $listData=ArrayHelper::map($events,'idEvent','event_name');


     echo $form->field($model, 'idEvent')->dropDownList($listData, ['id'=>'idEvent'])->label('Event');

    ?>

    <?php
     echo $form->field($model, 'idAttendant')->widget(DepDrop::classname(), [
        'type'=>DepDrop::TYPE_SELECT2,
        'options'=>['id'=>'idAttendant', 'class'=>'form-control'],
        //'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
        'pluginOptions'=>[
            'depends'=>['idEvent'],
            'placeholder'=>'Select an Attendant',
            'url'=>Url::to(['/event-registration/sample']),
        'initialize'=>true,
    ]])->label('Attendant');
    ?>

    <?php
        echo $form->errorSummary($model,['header'=>'Error:'])

    ?>

    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- event-registration-break -->

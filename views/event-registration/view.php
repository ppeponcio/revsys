<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EventRegistration */

$this->title = $model->idAttendant;
$this->params['breadcrumbs'][] = ['label' => 'Event Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-registration-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idAttendant' => $model->idAttendant, 'idEvent' => $model->idEvent], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idAttendant' => $model->idAttendant, 'idEvent' => $model->idEvent], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguró?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'attendant.first_name',
            'attendant.last_name',
            'attendant.passport',
            'event.event_name',
            'registration_date',
            'user.username',
            'barcode',
            //'material:boolean',
            [
             'attribute' => 'material',
             'value' => $model->material,
             'format' => 'boolean',
             'visible' => $model->event->material,
             ],
            [
             'attribute' => 'break_number',
             'value' => $model->break_number,
             'visible' => $model->event->breaks,
             ],
            [
             'attribute' => 'Break Total',
             'value' => $model->event->break_quantity,
             'visible' => $model->event->breaks,
             ],
             [
                'attribute' => 'Total Attended Conferences',
                'value' => count($model->attendant->conferences),
             ],
             [
                'attribute' => 'Attended Conferences',
                'value' => implode(', ',\yii\helpers\ArrayHelper::map($model->attendant->conferences, 'idConference', 'conference_name')),
                'visible' => count($model->attendant->conferences)>0,
             ],

        ],
    ]) ?>

</div>

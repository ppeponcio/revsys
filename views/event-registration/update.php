<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventRegistration */

$this->title = 'Update Event Registration: ' . $model->idAttendant;
$this->params['breadcrumbs'][] = ['label' => 'Event Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idAttendant, 'url' => ['view', 'idAttendant' => $model->idAttendant, 'idEvent' => $model->idEvent]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-registration-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
